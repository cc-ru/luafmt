macro_rules! define_puncts {
    ($( $i:ident => $s:literal ),+) => {
        #[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
        pub enum Punct {
            $( $i, )+
        }

        impl Punct {
            pub fn parse(s: &str) -> Option<Punct> {
                match s {
                    $( $s => Some(Punct::$i), )+
                    _ => None,
                }
            }

            pub fn as_str(&self) -> &'static str {
                match self {
                    $( Punct::$i => $s, )+
                }
            }
        }
    };
}

define_puncts! {
    Add => "+",
    Subtract => "-",
    Multiply => "*",
    Divide => "/",
    Modulo => "%",
    Power => "^",
    Length => "#",
    BitAnd => "&",
    Tilde => "~",
    BitOr => "|",
    LeftShift => "<<",
    RightShift => ">>",
    IntDiv => "//",
    Equals => "==",
    NotEquals => "~=",
    LessEqual => "<=",
    GreaterEqual => ">=",
    Less => "<",
    Greater => ">",
    Assign => "=",
    LeftParen => "(",
    RightParen => ")",
    LeftBrace => "{",
    RightBrace => "}",
    LeftBracket => "[",
    RightBracket => "]",
    Label => "::",
    Semicolon => ";",
    Colon => ":",
    Comma => ",",
    Period => ".",
    Concat => "..",
    Dots => "..."
}
