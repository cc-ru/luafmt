use std::io::{self, Read};

use luafmt::lexer::Lexer;
use luafmt::Token;

fn main() {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input).unwrap();

    println!();

    let mut lexer = Lexer::new();

    loop {
        match lexer.next_token(input.as_str()) {
            Ok(Token::EndOfFile) => break,
            Ok(tok) => println!("{:?}", tok),
            Err(e) => {
                eprintln!("{}", e.show("input.lua", input.as_str()));
                break;
            }
        }
    }
}
