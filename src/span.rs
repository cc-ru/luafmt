use std::fmt::{self, Display};

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Position {
    pub byte: usize,
    pub line: u32,
    pub column: u32,
}

impl Position {
    pub fn new(byte: usize, line: u32, column: u32) -> Position {
        Position { byte, line, column }
    }

    pub fn next_column(&mut self) {
        self.column += 1;
    }

    pub fn next_line(&mut self) {
        self.line += 1;
        self.column = 1;
    }
}

impl Display for Position {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}:{}", self.line, self.column)
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Span {
    pub start: Position,
    pub end: Position,
}

impl Span {
    pub fn new(start: Position, end: Position) -> Span {
        Span { start, end }
    }

    pub fn single(pos: Position) -> Span {
        Span::new(pos, pos)
    }

    pub fn substr(self, slice: &str) -> &str {
        &slice[self.start.byte..self.end.byte]
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct Spanned<T> {
    pub inner: T,
    pub span: Span,
}

impl<T> Spanned<T> {
    pub fn new(inner: T, span: Span) -> Spanned<T> {
        debug_assert!(span.end >= span.start);
        Spanned { inner, span }
    }

    pub fn substr<'a>(&self, slice: &'a str) -> &'a str {
        self.span.substr(slice)
    }
}
