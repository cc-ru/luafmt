#![deny(rust_2018_idioms)]
#![deny(unsafe_code)]

mod keyword;
mod punct;
mod span;

pub mod error;
pub mod lexer;

pub use error::Error;
pub use keyword::Keyword;
pub use lexer::Token;
pub use punct::Punct;
pub use span::{Position, Span, Spanned};
