use std::borrow::Cow;

use crate::{Error, Keyword, Position, Punct, Span, Spanned};

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum LiteralString<'a> {
    Short(Cow<'a, str>),
    Long { data: &'a str, level: u8 },
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum LiteralNumber {
    Integer(i64),
    Float(f64),
}

#[derive(Clone, Debug, PartialEq)]
pub enum Token<'a> {
    Name(&'a str),
    Keyword(Keyword),
    Punct(Punct),
    Number(LiteralNumber),
    String(LiteralString<'a>),
    Whitespace,
    Newline,
    Comment(&'a str),
    EndOfFile,
}

fn is_newline(c: char) -> bool {
    match c {
        '\r' => true,       // carriage return
        '\n' => true,       // line feed
        '\u{000b}' => true, // vertical tab
        '\u{000c}' => true, // form feed
        '\u{0085}' => true, // next line
        '\u{2028}' => true, // line separator
        '\u{2029}' => true, // paragraph separator
        _ => false,
    }
}

fn is_name_start(c: char) -> bool {
    c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c == '_'
}

fn is_name_cont(c: char) -> bool {
    is_name_start(c) || c >= '0' && c <= '9'
}

fn simple_escape_code(c: char) -> Option<char> {
    match c {
        'a' => Some('\u{0007}'), // bell
        'b' => Some('\u{0008}'), // backspace
        'f' => Some('\u{000c}'), // form feed
        'n' => Some('\u{000a}'), // line feed
        'r' => Some('\u{000d}'), // carriage return
        't' => Some('\u{0009}'), // horizontal tab
        'v' => Some('\u{000b}'), // vertical tab
        '"' => Some('"'),
        '\'' => Some('\''),
        _ => None,
    }
}

type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub struct Lexer {
    position: Position,
    prev_pos: Option<Position>,
}

impl Default for Lexer {
    fn default() -> Lexer {
        Lexer::new()
    }
}

macro_rules! bail {
    ($pos:expr, $( $t:tt )+) => {
        return Err(Error::new(Span::single($pos), format!($( $t )+)));
    };
}

impl Lexer {
    pub fn new() -> Lexer {
        Lexer {
            position: Position::new(0, 1, 1),
            prev_pos: None,
        }
    }

    pub fn next_token<'a>(&mut self, input: &'a str) -> Result<Token<'a>> {
        let token = match self.peek_char(input) {
            Some(c) if c.is_digit(10) => self.parse_number(input)?,
            Some('\'') | Some('"') => self.parse_short_string(input)?,
            Some('[') => self.parse_long_string(input)?,
            Some(c) if is_name_start(c) => self.parse_name(input),
            Some(c) if is_newline(c) => self.parse_newline(input),
            Some(c) if c.is_whitespace() => self.parse_whitespace(input),
            Some('-') if self.not_parsed(input).starts_with("--") => self.parse_comment(input)?,
            Some(_) => match self.try_parse_punct(input) {
                Some(v) => v,
                None => bail!(self.position, "unexpected character"),
            },
            None => Token::EndOfFile,
        };

        Ok(token)
    }

    pub fn next_token_spanned<'a>(&mut self, input: &'a str) -> Result<Spanned<Token<'a>>> {
        let start = self.position;
        let token = self.next_token(input)?;
        let end = self.prev_pos.unwrap();

        Ok(Spanned::new(token, Span::new(start, end)))
    }

    fn parse_short_string<'a>(&mut self, input: &'a str) -> Result<Token<'a>> {
        let start = self.position;
        let quote = self.next_char(input);

        let borrowed_start = self.position.byte;
        let mut borrowed_end = borrowed_start;
        let mut string = Cow::from("");

        let mut closed = false;

        while let Some(c) = self.next_char(input) {
            if Some(c) == quote {
                closed = true;
                break;
            }

            if is_newline(c) {
                bail!(self.prev_pos.unwrap(), "unfinished short string literal");
            }

            if c == '\\' {
                self.parse_escape_sequence(input, string.to_mut())?;
            } else if let Cow::Borrowed(ref mut string) = string {
                borrowed_end += c.len_utf8();
                *string = &input[borrowed_start..borrowed_end];
            } else {
                string.to_mut().push(c);
            }
        }

        if !closed {
            bail!(start, "unfinished short string literal");
        }

        if let Cow::Owned(ref mut string) = string {
            string.shrink_to_fit();
        }

        Ok(Token::String(LiteralString::Short(string)))
    }

    fn parse_escape_sequence(&mut self, input: &str, string: &mut String) -> Result<()> {
        let next = match self.peek_char(input) {
            Some(v) => v,
            None => bail!(self.position, "escape sequence expected, found end of file"),
        };

        match next {
            'x' => self.parse_hex_escape(input, string)?,
            'u' => self.parse_unicode_escape(input, string)?,
            'z' => self.parse_whitespace_escape(input),
            c if is_newline(c) => self.parse_newline_escape(input, string),
            _ => {
                if let Some(unescaped) = simple_escape_code(next) {
                    self.next_char(input);
                    string.push(unescaped);
                } else {
                    bail!(self.position, "invalid escape sequence")
                }
            }
        }

        Ok(())
    }

    fn parse_hex_escape(&mut self, input: &str, string: &mut String) -> Result<()> {
        let start = self.prev_pos.unwrap();
        self.next_char(input);

        let unescaped = self
            .next_char(input)
            .and_then(|c| c.to_digit(16))
            .and_then(|fst| {
                self.next_char(input)
                    .and_then(|c| c.to_digit(16))
                    .map(|snd| fst * 16 + snd)
                    .and_then(std::char::from_u32)
            });

        match unescaped {
            Some(v) => string.push(v),
            c => bail!(start, "expected '{{', found {}", describe_char(c)),
        }

        Ok(())
    }

    fn parse_unicode_escape(&mut self, input: &str, string: &mut String) -> Result<()> {
        let start = self.prev_pos.unwrap();
        self.next_char(input);

        match self.peek_char(input) {
            Some('{') => (),
            c => bail!(self.position, "expected '{{', found {}", describe_char(c)),
        }

        self.next_char(input);

        let mut code = 0u32;
        let mut found_digit = false;

        while let Some(digit) = self.peek_char(input).and_then(|c| c.to_digit(16)) {
            self.next_char(input);

            found_digit = true;

            code = if let Some(code) = code
                .checked_mul(16)
                .and_then(|code| code.checked_add(digit))
            {
                code
            } else {
                bail!(start, "invalid unicode escape sequence");
            }
        }

        if !found_digit {
            bail!(start, "invalid unicode escape sequence");
        }

        let char = match std::char::from_u32(code) {
            Some(v) => v,
            None => bail!(start, "invalid unicode escape sequence"),
        };

        string.push(char);

        match self.peek_char(input) {
            Some('}') => (),
            c => bail!(self.position, "expected '}}', found {}", describe_char(c)),
        }

        self.next_char(input);

        Ok(())
    }

    fn parse_whitespace_escape(&mut self, input: &str) {
        self.next_char(input);

        while self
            .peek_char(input)
            .filter(|c| c.is_whitespace())
            .is_some()
        {
            self.next_char(input);
        }
    }

    fn parse_newline_escape(&mut self, input: &str, string: &mut String) {
        if self.not_parsed(input).starts_with("\r\n") {
            self.next_char(input);
            self.next_char(input);
            string.push_str("\r\n");
        } else {
            string.push(self.next_char(input).unwrap());
        }
    }

    fn parse_long_string<'a>(&mut self, input: &'a str) -> Result<Token<'a>> {
        let span_start = self.position;
        self.next_char(input);
        let mut level = 0u8;

        while let Some('=') = self.peek_char(input) {
            level += 1;
            self.next_char(input);
        }

        let level = level;

        match self.next_char(input) {
            Some('[') => (),
            c => bail!(self.position, "expected '[', found {}", describe_char(c)),
        }

        let start = self.position.byte;
        let mut end = start;

        'outer: loop {
            match self.next_char(input) {
                Some(']') => {
                    for i in 0..level {
                        match self.next_char(input) {
                            Some('=') => (),
                            _ => {
                                end += 2 + usize::from(i);
                                continue 'outer;
                            }
                        }
                    }

                    match self.next_char(input) {
                        Some(']') => {
                            break Ok(Token::String(LiteralString::Long {
                                level,
                                data: &input[start..end],
                            }));
                        }

                        _ => {
                            end += 3 + usize::from(level);
                        }
                    }
                }

                Some(c) => end += c.len_utf8(),
                None => bail!(span_start, "unfinished long string literal"),
            }
        }
    }

    fn parse_number<'a>(&mut self, input: &'a str) -> Result<Token<'a>> {
        let is_hex =
            self.not_parsed(input).starts_with("0x") || self.not_parsed(input).starts_with("0X");

        let radix = if is_hex { 16 } else { 10 };

        if is_hex {
            self.next_char(input);
            self.next_char(input);
        }

        let integer_part = if radix == 10 {
            self.parse_decimal_integer(input)
        } else {
            LiteralNumber::Integer(self.parse_hex_integer(input))
        };

        let number = if let Some('.') = self.peek_char(input) {
            self.next_char(input);
            let float_part = self.parse_float_part(input, radix);

            LiteralNumber::Float(match integer_part {
                LiteralNumber::Integer(i) => (i as f64) + float_part,
                LiteralNumber::Float(f) => f + float_part,
            })
        } else {
            integer_part
        };

        let exponent = match self.peek_char(input) {
            Some('e') | Some('E') if radix == 10 => {
                self.next_char(input);
                10f64.powf(self.parse_exponent(input, radix)?)
            }

            Some('p') | Some('P') if radix == 16 => {
                self.next_char(input);
                2f64.powf(self.parse_exponent(input, radix)?)
            }

            _ => {
                return Ok(Token::Number(number));
            }
        };

        let f = match number {
            LiteralNumber::Integer(i) => (i as f64) * exponent,
            LiteralNumber::Float(f) => f * exponent,
        };

        Ok(Token::Number(LiteralNumber::Float(f)))
    }

    fn parse_decimal_integer<'a>(&mut self, input: &'a str) -> LiteralNumber {
        let mut integer = LiteralNumber::Integer(0);

        while let Some(c) = self.peek_char(input) {
            if let Some(digit) = c.to_digit(10) {
                self.next_char(input);

                integer = match integer {
                    LiteralNumber::Integer(i) => i
                        .checked_mul(10)
                        .and_then(|i| i.checked_add(i64::from(digit)))
                        .map(LiteralNumber::Integer)
                        .unwrap_or_else(|| {
                            LiteralNumber::Float((i as f64) * 10.0 + f64::from(digit))
                        }),

                    LiteralNumber::Float(f) => LiteralNumber::Float(f * 10.0 + f64::from(digit)),
                }
            } else {
                break;
            }
        }

        integer
    }

    fn parse_hex_integer<'a>(&mut self, input: &'a str) -> i64 {
        let mut integer = 0i64;

        while let Some(c) = self.peek_char(input) {
            if let Some(digit) = c.to_digit(16) {
                self.next_char(input);

                integer = integer.wrapping_mul(16).wrapping_add(i64::from(digit));
            } else {
                break;
            }
        }

        integer
    }

    fn parse_float_part<'a>(&mut self, input: &'a str, radix: u32) -> f64 {
        let mut num = 0.0;
        let mut denom = 1.0;

        while let Some(c) = self.peek_char(input) {
            if let Some(digit) = c.to_digit(radix) {
                self.next_char(input);
                denom *= f64::from(radix);
                num *= f64::from(radix);
                num += f64::from(digit);
            } else {
                break;
            }
        }

        num / denom
    }

    fn parse_exponent<'a>(&mut self, input: &'a str, radix: u32) -> Result<f64> {
        let is_negative = match self.peek_char(input) {
            Some('+') => {
                self.next_char(input);
                false
            }

            Some('-') => {
                self.next_char(input);
                true
            }

            _ => false,
        };

        let mut exponent = 0.0;
        let mut found = false;

        loop {
            match self.peek_char(input) {
                Some(c) if c.is_digit(radix) => {
                    let digit = c.to_digit(radix).unwrap();
                    self.next_char(input);
                    exponent *= f64::from(radix);
                    exponent += f64::from(digit);
                    found = true;
                }

                c => {
                    if found {
                        break;
                    }

                    bail!(
                        self.position,
                        "expected one of `+`, `-`, or a decimal digit, found {}",
                        describe_char(c)
                    );
                }
            }
        }

        if is_negative {
            Ok(-exponent)
        } else {
            Ok(exponent)
        }
    }

    fn parse_comment<'a>(&mut self, input: &'a str) -> Result<Token<'a>> {
        self.next_char(input);
        self.next_char(input);

        if let Some('[') = self.peek_char(input) {
            match self.parse_long_string(input)? {
                Token::String(LiteralString::Long { data, .. }) => Ok(Token::Comment(data)),
                _ => unreachable!(),
            }
        } else {
            let start = self.position.byte;
            let mut end = start;

            while let Some(c) = self.peek_char(input) {
                if is_newline(c) {
                    break;
                } else {
                    self.next_char(input);
                    end += c.len_utf8();
                }
            }

            Ok(Token::Comment(&input[start..end]))
        }
    }

    fn parse_name<'a>(&mut self, input: &'a str) -> Token<'a> {
        let start = self.position.byte;
        let mut end = start;

        while let Some(c) = self.peek_char(input) {
            if !is_name_cont(c) {
                break;
            }

            self.next_char(input);
            end += c.len_utf8();
        }

        let name = &input[start..end];

        if let Some(keyword) = Keyword::parse(name) {
            Token::Keyword(keyword)
        } else {
            Token::Name(name)
        }
    }

    fn try_parse_punct<'a>(&mut self, input: &'a str) -> Option<Token<'a>> {
        let not_parsed = self.not_parsed(input);

        Some(3)
            .filter(|&i| not_parsed.is_char_boundary(i))
            .and_then(|_| Punct::parse(&not_parsed[0..3]))
            .map(|op| {
                self.advance(input, 3);
                op
            })
            .or_else(|| {
                Some(2)
                    .filter(|&i| not_parsed.is_char_boundary(i))
                    .and_then(|_| Punct::parse(&not_parsed[0..2]))
                    .map(|op| {
                        self.advance(input, 2);
                        op
                    })
            })
            .or_else(|| {
                Some(1)
                    .filter(|&i| not_parsed.is_char_boundary(i))
                    .and_then(|_| Punct::parse(&not_parsed[0..1]))
                    .map(|op| {
                        self.advance(input, 1);
                        op
                    })
            })
            .map(Token::Punct)
    }

    fn parse_whitespace<'a>(&mut self, input: &'a str) -> Token<'a> {
        while let Some(c) = self.peek_char(input) {
            if !c.is_whitespace() {
                break;
            }

            self.next_char(input);
        }

        Token::Whitespace
    }

    fn parse_newline<'a>(&mut self, input: &'a str) -> Token<'a> {
        if let (Some('\r'), Some('\n')) = (self.next_char(input), self.peek_char(input)) {
            self.next_char(input);
        }

        Token::Newline
    }

    fn not_parsed<'a>(&self, input: &'a str) -> &'a str {
        &input[self.position.byte..]
    }

    fn peek_char(&self, input: &str) -> Option<char> {
        self.not_parsed(input).chars().nth(0)
    }

    fn next_char(&mut self, input: &str) -> Option<char> {
        if let Some(c) = self.peek_char(input) {
            let next_byte = self.position.byte + 1;
            self.position.byte += c.len_utf8();
            self.prev_pos = Some(self.position);

            if is_newline(c) {
                let skip = if c == '\r' && input.is_char_boundary(next_byte) {
                    input.as_bytes()[next_byte] == b'\n'
                } else {
                    false
                };

                if !skip {
                    self.position.next_line()
                } else {
                    self.position.next_column();
                }
            } else {
                self.position.next_column();
            }

            Some(c)
        } else {
            None
        }
    }

    fn advance(&mut self, input: &str, count: usize) {
        for _ in 0..count {
            self.next_char(input);
        }
    }
}

fn describe_char(c: Option<char>) -> String {
    use unicode_names2::name;

    match c {
        Some(c) if is_newline(c) => "newline".to_string(),

        Some(c) if c.is_control() => {
            if let Some(name) = name(c) {
                format!("control character U+{:04X} ({})", c as u32, name)
            } else {
                format!("control character U+{:04X}", c as u32)
            }
        }

        Some(c) => {
            if let Some(name) = name(c) {
                format!("`{}` U+{:04X} ({})", c, c as u32, name)
            } else {
                format!("`{}` U+{:04X}", c, c as u32)
            }
        }

        None => "end of file".to_string(),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn assert_parses(input: &str, token: Token<'_>, substr: &str) {
        let mut lexer = Lexer::new();
        let actual_token = lexer.next_token_spanned(input).unwrap();
        assert_eq!(actual_token.inner, token);
        assert_eq!(actual_token.substr(input), substr);
    }

    #[test]
    fn parse_name() {
        assert_parses("hello_123;", Token::Name("hello_123"), "hello_123");
    }

    #[test]
    fn parse_keyword() {
        assert_parses("for;", Token::Keyword(Keyword::For), "for");
    }

    #[test]
    fn parse_whitespace() {
        assert_parses("  \t  ;", Token::Whitespace, "  \t  ");
        assert_parses("    ;", Token::Whitespace, "    ");
    }

    #[test]
    fn parse_short_string() {
        assert_parses(
            r#""hello")"#,
            Token::String(LiteralString::Short("hello".into())),
            r#""hello""#,
        );

        assert_parses(
            r#""hello\r\n")"#,
            Token::String(LiteralString::Short("hello\r\n".into())),
            r#""hello\r\n""#,
        );

        assert_parses(
            r#""hello\x30")"#,
            Token::String(LiteralString::Short("hello0".into())),
            r#""hello\x30""#,
        );

        assert_parses(
            r#""he\u{006c}lo")"#,
            Token::String(LiteralString::Short("hello".into())),
            r#""he\u{006c}lo""#,
        );

        assert_parses(
            r#""hell\z   o")"#,
            Token::String(LiteralString::Short("hello".into())),
            r#""hell\z   o""#,
        );
    }

    #[test]
    fn parse_long_string() {
        assert_parses(
            r#"[[goodbye cruel world \o]] -- not a song reference"#,
            Token::String(LiteralString::Long {
                level: 0,
                data: r#"goodbye cruel world \o"#,
            }),
            r#"[[goodbye cruel world \o]]"#,
        );

        assert_parses(
            r#"[===[goodbye cruel world ]==] \o]===];"#,
            Token::String(LiteralString::Long {
                level: 3,
                data: r#"goodbye cruel world ]==] \o"#,
            }),
            r#"[===[goodbye cruel world ]==] \o]===]"#,
        );
    }

    #[test]
    fn parse_number() {
        assert_parses("123+", Token::Number(LiteralNumber::Integer(123)), "123");

        assert_parses(
            "123.456+",
            Token::Number(LiteralNumber::Float(123.456)),
            "123.456",
        );

        assert_parses(
            "12.34e2+",
            Token::Number(LiteralNumber::Float(1234.0)),
            "12.34e2",
        );

        assert_parses(
            "0.1e-3;",
            Token::Number(LiteralNumber::Float(0.0001)),
            "0.1e-3",
        );

        assert_parses(
            "0x1p8;",
            Token::Number(LiteralNumber::Float(256.0)),
            "0x1p8",
        );

        assert_parses(
            "0x1p-8;",
            Token::Number(LiteralNumber::Float(0.00390625)),
            "0x1p-8",
        );

        assert_parses(
            "0x1.5p8;",
            Token::Number(LiteralNumber::Float(336.0)),
            "0x1.5p8",
        );

        assert_parses(
            "0x7FFFFFFFFFFFFFFF;",
            Token::Number(LiteralNumber::Integer(0x7FFFFFFFFFFFFFFF)),
            "0x7FFFFFFFFFFFFFFF",
        );

        assert_parses(
            "0x8000000000000000;",
            Token::Number(LiteralNumber::Integer(-0x8000000000000000)),
            "0x8000000000000000",
        );

        assert_parses(
            "9223372036854775808;",
            Token::Number(LiteralNumber::Float(9.223372036854776e+18)),
            "9223372036854775808",
        );
    }

    #[test]
    fn parse_comment() {
        assert_parses(
            "-- variable called i\ni = 5",
            Token::Comment(" variable called i"),
            "-- variable called i",
        );

        assert_parses(
            "--[[\nvariable called i\n]]\ni = 5",
            Token::Comment("\nvariable called i\n"),
            "--[[\nvariable called i\n]]",
        );
    }

    #[test]
    fn parse_punct() {
        assert_parses("+5", Token::Punct(Punct::Add), "+");
        assert_parses(">>5", Token::Punct(Punct::RightShift), ">>");
        assert_parses(".rip", Token::Punct(Punct::Period), ".");
        assert_parses("..'rip'", Token::Punct(Punct::Concat), "..");
        assert_parses("...", Token::Punct(Punct::Dots), "...");
    }

    #[test]
    fn parse_newline() {
        assert_parses("\nhello", Token::Newline, "\n");
        assert_parses("\r\ngoodbye", Token::Newline, "\r\n");
        assert_parses("\r\n\n\n\n", Token::Newline, "\r\n");
    }

    #[test]
    fn parse_code() {
        let mut lexer = Lexer::new();
        let input = r#"for i = 1, 10 do print("hello " .. i) end"#;
        let mut tokens = vec![];

        loop {
            match lexer.next_token(input) {
                Ok(Token::EndOfFile) => break,
                tok => tokens.push(tok.unwrap()),
            }
        }

        assert_eq!(
            tokens,
            &[
                Token::Keyword(Keyword::For),
                Token::Whitespace,
                Token::Name("i".into()),
                Token::Whitespace,
                Token::Punct(Punct::Assign),
                Token::Whitespace,
                Token::Number(LiteralNumber::Integer(1)),
                Token::Punct(Punct::Comma),
                Token::Whitespace,
                Token::Number(LiteralNumber::Integer(10)),
                Token::Whitespace,
                Token::Keyword(Keyword::Do),
                Token::Whitespace,
                Token::Name("print".into()),
                Token::Punct(Punct::LeftParen),
                Token::String(LiteralString::Short("hello ".into())),
                Token::Whitespace,
                Token::Punct(Punct::Concat),
                Token::Whitespace,
                Token::Name("i".into()),
                Token::Punct(Punct::RightParen),
                Token::Whitespace,
                Token::Keyword(Keyword::End),
            ]
        );
    }
}
