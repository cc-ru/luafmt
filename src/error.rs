use std::fmt::{self, Display};
use unicode_width::UnicodeWidthChar;

use crate::Span;

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum Severity {
    Error,
    Warning,
}

impl Display for Severity {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter.write_str(match *self {
            Severity::Error => "error",
            Severity::Warning => "warning",
        })
    }
}

#[derive(Clone, Debug)]
pub struct Error {
    pub span: Span,
    pub message: String,
    pub severity: Severity,
}

impl Error {
    pub fn new(span: Span, message: impl ToString) -> Error {
        Error {
            span,
            message: message.to_string(),
            severity: Severity::Error,
        }
    }

    pub fn with_severity(mut self, severity: Severity) -> Self {
        self.severity = severity;
        self
    }

    pub fn show(&self, filename: &str, input: &str) -> String {
        format!(
            "{}: {}\n{}",
            self.severity,
            self.message,
            show_span(filename, input, self.span)
        )
    }
}

fn show_span(filename: &str, input: &str, span: Span) -> String {
    let num_lines = (span.end.line - span.start.line + 1) as usize;
    let pad = num_digits(span.end.line as usize);
    let pad_spaces = " ".repeat(pad);

    let headline = format!("{}--> {}:{}\n", pad_spaces, filename, span.start);

    let code = input
        .lines()
        .enumerate()
        .skip(span.start.line as usize - 1)
        .take(num_lines)
        .map(|(i, line)| {
            let i = i + 1;

            if i == span.start.line as usize {
                let left_pad = line
                    .chars()
                    .take((span.start.column - 1) as usize)
                    .map(|c| c.width().unwrap_or(0))
                    .sum();
                let left_pad = " ".repeat(left_pad);

                let cont = line
                    .chars()
                    .take(if span.end.line == span.start.line {
                        (span.end.column - span.start.column) as usize
                    } else {
                        line.len()
                    })
                    .map(|c| c.width().unwrap_or(0))
                    .sum();
                let cont = "~".repeat(cont);

                format!(
                    "{:.*} | {}\n{} | {}^{}",
                    pad, i, line, pad_spaces, left_pad, cont,
                )
            } else {
                format!("{:.*} | {}\n", pad, i, line)
            }
        });

    Some(headline).into_iter().chain(code).collect::<String>()
}

fn num_digits(n: usize) -> usize {
    ((n as f64).log10() + 1.0) as usize
}
