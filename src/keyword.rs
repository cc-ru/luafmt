macro_rules! define_keywords {
    ($( $i:ident => $s:literal ),+) => {
        #[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
        pub enum Keyword {
            $( $i, )+
        }

        impl Keyword {
            pub fn parse(s: &str) -> Option<Keyword> {
                match s {
                    $( $s => Some(Keyword::$i), )+
                    _ => None,
                }
            }

            pub fn as_str(&self) -> &'static str {
                match self {
                    $( Keyword::$i => $s, )+
                }
            }
        }
    };
}

define_keywords! {
    And => "and",
    Break => "break",
    Do => "do",
    Else => "else",
    ElseIf => "elseif",
    End => "end",
    False => "false",
    For => "for",
    Function => "function",
    Goto => "goto",
    If => "if",
    In => "in",
    Local => "local",
    Nil => "nil",
    Not => "not",
    Or => "or",
    Repeat => "repeat",
    Return => "return",
    Then => "then",
    True => "true",
    Until => "until",
    While => "while"
}
